// Add additional field percentage to each object.

const data = [
  {
    id: 23243204,
    name: "Max",
    marks: {
      english: 52,
      maths: 26,
      science: 39,
    },
  },
  {
    id: 88243204,
    name: "Trixie",
    marks: {
      english: 39,
      maths: 29,
      science: 49,
    },
  },
  {
    id: 78363204,
    name: "Spectre",
    marks: {
      english: 65,
      maths: 92,
      science: 79,
    },
  },
  {
    id: 79363994,
    name: "Olivia",
    marks: {
      english: 75,
      maths: 75,
      science: 73,
    },
  },
  {
    id: 7930004,
    name: "Gerrard",
    marks: {
      english: 70,
      maths: 82,
      science: 93,
    },
  },
];
