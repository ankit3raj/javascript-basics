const data = [
  {
    id: 23243204,
    name: "Max",
    marks: {
      english: 52,
      maths: 26,
      science: 39,
    },
  },
  {
    id: 88243204,
    name: "Trixie",
    marks: {
      english: 39,
      maths: 29,
      science: 49,
    },
  },
  {
    id: 78363204,
    name: "Spectre",
    marks: {
      english: 65,
      maths: 92,
      science: 79,
    },
  },
  {
    id: 79363994,
    name: "Olivia",
    marks: {
      english: 75,
      maths: 75,
      science: 73,
    },
  },
  {
    id: 7930004,
    name: "Gerrard",
    marks: {
      english: 70,
      maths: 82,
      science: 93,
    },
  },
];

/*
Note: Marks are given out of 100.
Marks < 33 = fail, Fail in any subject implies fail.


Q1. Generate following data structure using above data.
// const result = { StudentID: Percentage, StudentID: Percentage ...}
*/

const percentage = data.map((item) => {
  return (mypercentage = (
    ((item.marks.english + item.marks.maths + item.marks.science) / 300) *
    100
  ).toFixed(2));
});
// console.log(percentage);
// console.log(percentage);
// for (let i = 0; i < percentage.length; i++) {
//   console.log(percentage[i]);
// }
const result = data.reduce((acc, curr) => {
  const studID = curr.id;

  acc[studID] = {};
  return acc;
});

console.log(result);
