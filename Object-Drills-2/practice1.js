const data = [
  {
    id: 345432984,
    population: "3.5M",
    countryName: "Italy",
    continent: "Europe",
  },
  {
    id: 345112884,
    population: "1.2T",
    countryName: "China",
    continent: "Asia",
  },
  {
    id: 3898432984,
    population: "75B",
    countryName: "India",
    continent: "Asia",
  },
  {
    id: 3898432774,
    population: "12M",
    countryName: "Germany",
    continent: "Europe",
  },
];
function convertPopulation(population) {
  return population[population.length - 1] == "M"
    ? parseInt(population.slice(0, -1)) * 10
    : population[population.length - 1] == "B"
    ? parseInt(population.slice(0, -1)) * 100
    : parseInt(population.slice(0, -1)) * 1000;
}

// data.forEach((item) => {
//   item.population = convertPopulation(item.population);
// });
// console.log(data.sort((a, b) => b.population - a.population));

// [{
//   cont: {
//     id: {
//       pop, count, cont;
//     }
//   }
// }]
const result = data.reduce((acc, curr) => {
  const { continent, id, ...rest } = curr;
  if (curr[continent]) {
    acc[continent][id] = rest;
  } else {
    acc[continent] = {};
    acc[continent][id] = rest;
  }
  return acc;
}, []);

// console.log(result);

result.Asia["3898432984"].population = "3M";
console.log(result);
