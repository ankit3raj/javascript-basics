// const data = [
//   {
//     userId: 1,
//     id: 1,
//     title:
//       "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
//     body: "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
//   },
//   {
//     userId: 1,
//     id: 2,
//     title: "qui est esse",
//     body: "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla",
//   },
//   {
//     userId: 1,
//     id: 3,
//     title: "ea molestias quasi exercitationem repellat qui ipsa sit aut",
//     body: "et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut",
//   },
//   {
//     userId: 1,
//     id: 4,
//     title: "eum et est occaecati",
//     body: "ullam et saepe reiciendis voluptatem adipisci\nsit amet autem assumenda provident rerum culpa\nquis hic commodi nesciunt rem tenetur doloremque ipsam iure\nquis sunt voluptatem rerum illo velit",
//   },
//   {
//     userId: 1,
//     id: 5,
//     title: "nesciunt quas odio",
//     body: "repudiandae veniam quaerat sunt sed\nalias aut fugiat sit autem sed est\nvoluptatem omnis possimus esse voluptatibus quis\nest aut tenetur dolor neque",
//   },
//   {
//     userId: 3,
//     id: 6,
//     title: "dolorem eum magni eos aperiam quia",
//     body: "ut aspernatur corporis harum nihil quis provident sequi\nmollitia nobis aliquid molestiae\nperspiciatis et ea nemo ab reprehenderit accusantium quas\nvoluptate dolores velit et doloremque molestiae",
//   },
//   {
//     userId: 1,
//     id: 7,
//     title: "magnam facilis autem",
//     body: "dolore placeat quibusdam ea quo vitae\nmagni quis enim qui quis quo nemo aut saepe\nquidem repellat excepturi ut quia\nsunt ut sequi eos ea sed quas",
//   },
//   {
//     userId: 1,
//     id: 8,
//     title: "dolorem dolore est ipsam",
//     body: "dignissimos aperiam dolorem qui eum\nfacilis quibusdam animi sint suscipit qui sint possimus cum\nquaerat magni maiores excepturi\nipsam ut commodi dolor voluptatum modi aut vitae",
//   },
//   {
//     userId: 1,
//     id: 9,
//     title: "nesciunt iure omnis dolorem tempora et accusantium",
//     body: "consectetur animi nesciunt iure dolore\nenim quia ad\nveniam autem ut quam aut nobis\net est aut quod aut provident voluptas autem voluptas",
//   },
//   {
//     userId: 3,
//     id: 10,
//     title: "optio molestias id quia eum",
//     body: "quo et expedita modi cum officia vel magni\ndoloribus qui repudiandae\nvero nisi sit\nquos veniam quod sed accusamus veritatis error",
//   },
// ];

/*


    Q1. Print the last object of the data array without using a loop.

    Q2. Print the object having id = 9.

    Q3. Form two arrays.
        First Array should have first 5 objects. 
        Second Array should have the rest others.

    Q4. Form two arrays 
        First Array should contain all objects having user id 1
        Second Array should contain all objects having user id 3

    Q5. Sort array (Ascending Order)
        A. Based on length of body "key".
        B. Based on userIds .
        C. Based on title .
        D. Based on ids (Descending Order).
  */
//1
// console.log(data[data.length - 1]);

//2

// for (let i = 0; i < data.length; i++) {
//   if (data[i].id == 9) {
//     console.log(data[i]);
//   }
// }

// 3 Form two arrays.
// First Array should have first 5 objects.
//  Second Array should have the rest others.

// let arr1 = [];
// let arr2 = [];
// for (let i = 0; i < 5; i++) {
//   arr1.push(data[i]);
// }
// for (let i = 5; i < data.length; i++) {
//   arr2.push(data[i]);
// }
// console.log(arr1);
// console.log(arr2);

// // 4. Form two arrays
//   First Array should contain all objects having user id 1
//         Second Array should contain all objects having user id 3

// let arr3 = [];
// let arr4 = [];
// for (let i = 0; i < 5; i++) {
//   if (data[i].userId == 1) {
//     arr3.push(data[i]);
//   }
// }
// for (let i = 0; i < 5; i++) {
//   if (data[i].userId == 3) {
//     arr4.push(data[i]);
//   }
// }
// console.log(arr3);

// 5.  Q5. Sort array (Ascending Order)
// A. Based on length of body "key".
// B. Based on userIds .
// C. Based on title .
// D. Based on ids (Descending Order).

// B
// console.log(data.sort((a, b) => a.userId - b.userId));

// D. Based on ids (Descending Order).
// console.log(data.sort((a, b) => b.id - a.id));

// c
// function compareTitle(a, b) {
//   let comparison = 0;
//   const name1 = a.title;
//   const name2 = b.title;
//   if (name1 > name2) {
//     comparison = 1;
//   } else if (name1 < name2) {
//     comparison = -1;
//   }
//   return comparison;
// }
// console.log(data.sort(compareTitle));

//A . Based on length of body "key".

// console.log(data.sort((a, b) => a.body.length - b.body.length));

const data = {
  person_info_1: {
    profile: {
      fullName: "Javier Hernandez",
      nationality: {
        country: "Mexico",
      },
      tel: 904902394,
    },
    current_address: {
      current_city: {
        value: "Bangalore",
        zip: "399993",
      },
    },
  },

  person_info_2: {
    profile: {
      fullName: "Emily Spade",
      nationality: {
        country: "Norway",
      },
      tel: 309320239,
    },
    current_address: {
      current_city: {
        value: "Oslo",
        zip: "239292",
      },
    },
  },
  person_info_3: {
    profile: {
      fullName: "John Cigan",
      nationality: {
        country: "Turkey",
      },
      tel: 932483988,
    },
    current_address: {
      current_city: {
        value: "Istanbul",
        zip: "932099",
      },
    },
  },
  person_info_4: {
    profile: {
      fullName: "Marsh Hobbs",
      nationality: {
        country: "USA",
      },
      tel: 32043988,
    },
    current_address: {
      current_city: {
        value: "Istanbul",
        zip: "932099",
      },
    },
  },
};

/*
    Q1. Get all persons whose current city  is Istanbul .
        A. Get result in objects form 
        {
            person_info: {
                // that person info
            }
        }

        B. Get result in array Form
        [{// Info of the person }, { // Info of the person}]

        use Both filter and reduce.

    Q2. Copy data object properly without leaving any reference.

    Q3. Use map to get me all nationality and current location for each person.
        use person's name as key to store the person's nationality and current_address.
        */

console.log(data.person_info_1.profile);
//command +shift+p for format option

for (let elem in data) {
  for (let nextelem of elem) {
    for (let thirdelem of nextelem) {
      if (data.elem.nextelem.thirdelem.country == "Istanbul") {
        console.log(nextelem);
      }
    }
  }
}
