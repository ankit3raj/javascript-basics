// Q1. Convert the following object into an array.
//  { Max: 32, Asta: 14, Yuno: 16 }
//  to
//  [{ name: "Max", age: 32}, { name: "Asta", age 14}, { name: "Yuno", age: 16}];

/* const item = { Max: 32, Asta: 14, Yuno: 16 };
let result = [];

for (let elem of Object.keys(item)) {
  const obj = {};
  obj.name = elem;
  obj.age = item[elem];
  result.push(obj);
}
console.log(result);
*/

// console.log(Object.entries(item));
/*
2  Convert the following array into an object.
  [{ name: "Max", age: 32}, { name: "Asta", age 14}, { name: "Yuno", age: 16}];
  to 
   { Max: 32, Asta: 14, Yuno: 16 }
*/
/*
const item = [
  { name: "Max", age: 32 },
  { name: "Asta", age: 14 },
  { name: "Yuno", age: 16 },
];

const result = {};
for (let elem of item) {
  result[elem.name] = elem.age;
}
console.log(result);
*/
/*
3.Add a new property to each element of the following array.
PropertyName -      [adult: true / false  ( age > 18 ? true : false)]

 const arr = [{ name: "Max", age: 32}, { name: "Asta", age 14}, { name: "Yuno", age: 16}];
 */
/*
const arr = [
  { name: "Max", age: 32 },
  { name: "Asta", age: 14 },
  { name: "Yuno", age: 16 },
];

for (let elem of arr) {
  if (elem.age > 18) {
    elem.adult = true;
  } else {
    elem.adult = false;
  }
}
console.log(arr);
*/

/* 
const items = [
    { qty: 23, name: "Microphone", price: "34USD" }, 
    { qty: 4, name: "HeadSet", price: "14USD" }
    { qty: 2, name: "Camera", price: "48USD" }
]
A.  Use above `items` array to generate the below array
    Use reduce and map.
    const result = [
        { name: "Microphone", price: "34USD" }, 
        { name: "HeadSet", price: "14AUD" }
        { name: "Camera", price: "48EUR" }
    ]
*/
/*
const items = [
  { qty: 23, name: "Microphone", price: "34USD" },
  { qty: 4, name: "HeadSet", price: "14USD" },
  { qty: 2, name: "Camera", price: "48USD" },
];

for (let elem of items) {
  delete elem.qty;
}
console.log(items);
*/
/*
Use above `items` array to generate the following object.
    const result = {
        Microphone: {
            price: {
                amount: 34,
                currency: "USD"
            }        
        },
        HeadSet: {
            price: {
                amount: 14
                currency: "AUD"
            }
        },
        Camera: {
            price: {
                amount: 48,
                currency: "EUR"
            }
        }
    }
*/
/*
const items = [
  { qty: 23, name: "Microphone", price: "34USD" },
  { qty: 4, name: "HeadSet", price: "14USD" },
  { qty: 2, name: "Camera", price: "48USD" },
];
const splittingCurrencyFromPrice = (price) => {
  let result = { currency: "", amount: "" };
  for (let i = 0; i < price.length; i++) {
    let x = parseInt(price[i], 10);
    if (isNaN(x)) {
      result.currency += price[i];
    } else {
      result.amount += x.toString();
    }
  }
  return {
    currency: result.currency,
    amount: parseInt(result.amount || 0),
  };
};
const obj = {};
const obj1 = {};
for (let elem of items) {
  const name = elem.name;
  const { currency, amount } = splittingCurrencyFromPrice(elem.price);
  obj[name] = {
    currency: currency,
    amount: amount,
  };
}
console.log(obj);
*/
/*
DAY 2 QUES 1  Generate following data structure using above data.
const result = { StudentID: Percentage, StudentID: Percentage ...}
*/

const data = [
  {
    id: 23243204,
    name: "Max",
    marks: {
      english: 52,
      maths: 26,
      science: 39,
    },
  },
  {
    id: 88243204,
    name: "Trixie",
    marks: {
      english: 39,
      maths: 29,
      science: 49,
    },
  },
  {
    id: 78363204,
    name: "Spectre",
    marks: {
      english: 65,
      maths: 92,
      science: 79,
    },
  },
  {
    id: 79363994,
    name: "Olivia",
    marks: {
      english: 75,
      maths: 75,
      science: 73,
    },
  },
  {
    id: 7930004,
    name: "Gerrard",
    marks: {
      english: 70,
      maths: 82,
      science: 93,
    },
  },
];
/*
const obj = {};
for (let elem of data) {
  //   obj.stId = elem.id;
  obj[elem.id] = (
    ((elem.marks.english + elem.marks.maths + elem.marks.science) / 300) *
    100
  ).toFixed(2);
}
console.log(obj);
*/
// let res = data.reduce((acc, curr) => {

//   acc[curr.id] = (
//     ((curr.marks.english + curr.marks.maths + curr.marks.science) / 300) *
//     100
//   ).toFixed(2);
//   //   acc.push(obj);
//   return acc;
// }, {});
// console.log(res);

/*
Day2 ques2 Add additional field percentage to each object.
*/
/*
for (let elem of data) {
  elem.percentage = (
    ((elem.marks.english + elem.marks.maths + elem.marks.science) / 300) *
    100
  ).toFixed(2);
}
console.log(data);
*/
// let res = data.reduce((acc, curr) => {
//   const obj = { percentage: "" };
//   obj.percentage = (
//     ((curr.marks.english + curr.marks.maths + curr.marks.science) / 300) *
//     100
//   ).toFixed(2);
//   acc.push(obj.percentage);
//   return acc;
// }, []);
// console.log(res);
/*
Day 2 Ques 3.Group Students in terms of fail and pass.

const result = {
    fail: [{ // Student Details }, { // Student Details} ...],
    pass: [{ // Student Details}, { // Student Details} ...]
}
*/
/*
const obj = { fail: [], pass: [] };
for (let elem of data) {
  if (
    elem.marks.english < 34 ||
    elem.marks.maths < 34 ||
    elem.marks.science < 34
  ) {
    obj.fail.push(elem);
  } else {
    obj.pass.push(elem);
  }
}
console.log(obj);
*/
// const ans = data.reduce(
//   (acc, curr) => {
//     const out = Object.keys(curr.marks).filter((x) => {
//       return curr.marks[x] < 33;
//     });
//     if (out.length) {
//       acc.fail.push({ ...curr });
//     } else {
//       acc.pass.push({ ...curr });
//     }

//     return acc;
//   },
//   { fail: [], pass: [] }
// );
// console.log(ans);
/*
Day 2 ques 4 .Add 5 Marks to each of the subjects and group them in terms of fail and pass in following manner.
*/
/*
const obj = { fail: [], pass: [] };
for (let elem of data) {
  ["maths", "english", "science"].forEach((key) => (elem.marks[key] += 5));
  if (
    elem.marks.english >= 34 &&
    elem.marks.maths >= 34 &&
    elem.marks.science >= 34
  ) {
    obj.pass.push(elem);
  } else {
    obj.fail.push(elem);
  }
}
//   obj.pass.push(elem);

console.log(obj);
*/
/*
Day 2 Ques 5 Sort data array in terms of percentage scored by each student.
Also sort the subject marks..(Show lowest subject marks for each candidate first).
*/
/*
for (let elem of data) {
  elem.percentage = (
    ((elem.marks.english + elem.marks.maths + elem.marks.science) / 300) *
    100
  ).toFixed(2);
}
// console.log(data);
const result = data.sort((e1, e2) => e1.percentage - e2.percentage);

result.forEach(
  (e) => (e.marks = Object.entries(e.marks).sort((a, b) => a[1] - b[1]))
);
console.log(result);
// for (let elem of data) {
//   Object.entries(elem.marks).sort((a, b) => b[1] - a[1]);
// }
// console.log(data);
*/

const books = [
  {
    _id: 32140239409,
    name: "Lord of the Rings",
    publish_year: 1993,
    author: {
      id: "au_3429849283",
      name: "Justin Lagrange",
    },
  },
  {
    _id: 32140239499,
    name: "Narnia",
    publish_year: 1994,
    author: {
      id: "au_3429849483",
      name: "Justin Hemworth",
    },
  },
  {
    _id: 32888239409,
    name: "Titanic",
    publish_year: 1995,
    author: {
      id: "au_3429849483",
      name: "Justin Hemworth",
    },
  },
  {
    _id: 321923339409,
    name: "Snowwhite",
    publish_year: 1996,
    author: {
      id: "au_3499923283",
      name: "Monty",
    },
  },
  {
    _id: 42140239409,
    name: "Sherlock Holmes",
    publish_year: 1997,
    author: {
      id: "au_6629849283",
      name: "Barney Stinson",
    },
  },
];

/* 
Day3 Ques1 Add Price 'property' with value 200USD to each book.
*/
/*
books.forEach((item) => {
  item.price = "200USD";
});
console.log(books);
Object.keys(books).reduce((acc, curr) => {
  books[curr].price = "200USD";

  acc.push(books[curr]);
  return acc;
}, []);
console.log(books);
*/

/* 
Day 3 Ques2 Group books by authors.{ author1: [], author2: [], ...}
*/

// const res = books.reduce((acc, curr) => {
//   let authorname = curr.author.name;
//   if (acc[authorname]) {
//     acc[authorname].push(curr.name);
//   } else {
//     acc[authorname] = [curr.name];
//   }
//   return acc;
// }, {});
// console.log(res);

/* 
Day 3 Ques 3 Remove the 4th item from the array.
*/
// for (let elem of books) {
//   delete elem.author;
// }
// console.log(books);
// let res = Object.entries(books).reduce((acc, curr) => {
//   console.log(curr[1][4]);
//   // acc = delete curr.author;
//   //   return acc;
// });
// console.log(res);
/*
Day 3 Ques 4  Clone the books array properly
*/
// const books2 = [...books];
// console.log(books2);
/*
Day3 Ques5 Create a list of all the authors ( all unique authors).
*/
// let res = books.reduce((acc, curr) => {
//   let authorname = curr.author.name;
//   if (!books.includes(acc[authorname])) {
//     acc.push(authorname);
//   }
//   return acc;
// }, []);
// let res1 = new Set(res);
// console.log(res1);

// let obj = { a: "25M", b: "34B", c: "50T" };
