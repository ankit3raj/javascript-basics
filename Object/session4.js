const data = [
  {
    23021930923: {
      quantity: 2,
      price: "$3",
      name: "Pen",
    },
    23243243243: {
      quantity: 2,
      price: "$12",
      name: "Book",
    },
    092139029130: {
      quantity: 6,
      price: "$1",
      name: "Eraser",
    },
    839439829489: {
      quantity: 3,
      price: "$6",
      name: "Pencil Box",
    },
    23948234888: {
      quantity: 1,
      price: "$8",
      name: "Geometry Box",
    },
  },
];

/*
  Q1. use Reduce to calculate totalPrice of all the items 
  [ Total Price = each Item Price * quantity]

  Q2. Sort items based on price.

  Q3. const money = "$2930.229"

      - Split the decimal and integer part of the decimal.
      - Round the number to 2 decimal places

  Q4. Write a method to Check and see if an address is a valid IP Address?
    examples =    "0.0.0.0" ,  "343.2.1.4"

  Q5. write a single method to convert the following 
     { first: "max", middle: "zeT", last: "Payne" } to "Max Zet Payne"
     { first: "henry", "last": "arnolD"} to "Henry Arnold"

  Q6. Write a single method that takes 3 params
      (domain - string, security - boolean, port - number)
      port - default value 80
      security - default value 1
and do conversion
      (domain - "google.com", security - 1)
       output:      `https://google.com:80`
      
      (domain - "man-utd", security - 0, port - 3000)
      output:       `http://manutd.com:3000`

3
*/

//1. Total price
const totalPrice = Object.values(data[0])
  .map((d) => +d.price.slice(1) * d.quantity)
  .reduce((sum, e) => sum + e);

console.log(totalPrice);

// use Reduce to calculate totalPrice of all the items
// [ Total Price = each Item Price * quantity]

// let total = data.map((item)=> {
//     return data[item]
// })

// for (let i = 0; i < data.length; i++) {
//   for (let [key, value] of Object.entries(data[i])) {
//
//   }
// }

// let data2 = data[0];
// let total = Object.entries(data2).map((value) => {
//   return value;
// });
// console.log(total);

// 4.

// function ValidateIPaddress(ipaddress) {
//   if (
//     /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(
//       myForm.emailAddr.value
//     )
//   ) {
//     return true;
//   } else {
//     return false;
//   }
// }

// function ValidateIPaddress(ipaddress){

// }
//2
// console.log(data[0]);
// let mystr = "$3";
// console.log(mystr[1]);

// //3

// const money = "$2930.229";

//2 sort price

const priceSort = Object.values(data[0])
  .map((d) => +d.price.slice(1))
  .sort((a, b) => a - b);

console.log(priceSort);

//3
const money = "$2930.229";
let moneyInNumber = money.replace(/\$|,/g, "");
console.log(moneyInNumber.split("."));

//4

function ValidateIPaddress(ipaddress) {
  if (
    /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(
      ipaddress
    )
  ) {
    return true;
  }

  return false;
}

ValidateIPaddress("343.2.1.4");

//5
let obj = { first: "max", middle: "zeT", last: "Payne" };
console.log(
  obj.first[0].toUpperCase() +
    obj.first.slice(1) +
    " " +
    obj.middle[0].toUpperCase() +
    obj.middle.slice(1) +
    " " +
    obj.last[0].toUpperCase() +
    obj.last.slice(1)
);
//  + " " + obj.middle + " " + obj.last);

let obj1 = { first: "henry", last: "arnolD" };
console.log(
  obj1.first[0].toUpperCase() +
    obj1.first.slice(1) +
    " " +
    obj1.last[0].toUpperCase() +
    obj1.last.slice(1, 5) +
    obj1.last[5].toLowerCase()
);

//7 Add total property to each data item ..which is the total price of that product. [ use both map and reduce  ].

// const total = Object.values(data[0]).map((d) => +d.price.slice(1) * d.quantity);

for (let key of Object.keys(data[0])) {
  const elem = data[0][key];
  elem.total = +elem.price.slice(1) * elem.quantity;
}

console.log(data);
