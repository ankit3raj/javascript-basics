/*
if we order a pizza ,we get the order number i.e a promise to give the user a pizza. 

Note - Promise is an object and it takes a callback function.Now that callback function takes two argument,resolve and reject.

if the order is ready,we resolve the promise or if something went wrong ,we rejected the promise.
 
To access the value of promise,we use the 'then' Method.

Instead of making one variable and then returned that variable ,we can return the callback function.

when we chained the promise by 'then' method ,this is called a promise land which allows us to keep all of them one level deep .

if we want to run all promises run simultaneously,we will use promise.all which is a static method that takes an array of promises.

promise.all will take all the sub-promises and resolve them only when all are done.

promise.race will return the first promise which is resolved first .

if promise goes awry then we call the reject function.

To catch the error ,we chain the '.catch' in the end of then method.

if any promise is rejected then we after the rejecting the order ,promise will return the fulfilled.

if there is a multiple then method then we should put 'catch' method at the last .

promise.allsetteled can return the both rejected and resolved promises

*/

// function makePizza(){
//     const pizzaPromise = new Promise(function(resolve,reject){
//         resolve('order is ready');
//     });
//     return pizzaPromise;
// }
// const pizza = makePizza();
// console.log(pizza);

// function makePizza(toppings){
//     const pizzaPromise = new Promise(function(resolve,reject){
//         setTimeout(function(){
//              resolve(`here is your pizza with toppings : ${toppings.join(' ')}`)
//         },2000)

//     });
//     return pizzaPromise;
// }
// const pepperoniPromise = makePizza(['pepperoni']);
// const canadianPromise = makePizza(['peperoni','onion','mushrooms'])

// console.log(pepperoniPromise);
// console.log(canadianPromise);

// pepperoniPromise.then(function(pizza){
//     console.log("Ahh! Got it");
//     console.log(pizza);
// })
// function makePizza(toppings = []) {
//   return new Promise(function (resolve, reject) {
//     const timeToBake = 500 + toppings.length * 200;
//     setTimeout(function () {
//       resolve(`here is your pizza with toppings : ${toppings.join(" ")}`);
//     }, timeToBake);
//   });
// }

// makePizza(['pepperoni'])
// .then(function(pizza){
//     console.log(pizza);
//     return makePizza(['peperoni','onion','mushrooms'])
// })
// .then(function(pizza){
//     console.log(pizza)
//     return makePizza(['ham','cheese'])
// })
// .then(function(pizza){
//     console.log(pizza)
// })

//To run simultaneously
const pepperoniPromise = makePizza(["pepperoni"]);
const canadianPromise = makePizza(["peperoni", "onion", "mushrooms"]);

// const dinnerPromise = Promise.all([pepperoniPromise, canadianPromise]);

// dinnerPromise.then(pizzas => {
//     console.log(pizzas);
// })
// it gives an array of pizzas

//with destructuring
// // const dinnerPromise = Promise.all([pepperoniPromise, canadianPromise]);
// // dinnerPromise.then(function(pizzas){
// //     const [hottie,farmerPizza] = pizzas;
// //     console.log(hottie,farmerPizza)
// })
//it gives a string.

//without destructuring

// const dinnerPromise = Promise.all([pepperoniPromise, canadianPromise]);
// dinnerPromise.then(function([hottie,farmerPizza]){
//     console.log(hottie,farmerPizza)
// })
//it will give a string .

// const firstPizzaPromise = Promise.race([pepperoniPromise, canadianPromise]);

// firstPizzaPromise.then((pizzas) => {
//   console.log(pizzas);
// });

// Reject function

function makePizza(toppings = []) {
  return new Promise(function (resolve, reject) {
    if (toppings.includes("pineapple")) {
      reject("order rejected");
    } else {
      const timeToBake = 500 + toppings.length * 200;
      setTimeout(function () {
        resolve(`here is your pizza with toppings : ${toppings.join(" ")}`);
      }, timeToBake);
    }
  });
}

makePizza(["pineapple", "cheese"])
  .then((pizzas) => {
    console.log(pizzas);
  })
  .catch((err) => {
    console.log("Nooooo");
    console.log(err);
  });
