/*
Q.1 Write an login Function that takes username and returns user details 
if user is able to login else 
return { data: null }

Note: User is only able to login if that username exists in the data.json


Q.2 Write a function that verifies if the user's social network (facebook, discord, linkedIn)
Note: To verify if social network exists.. you must see if there is a corresponding username in the
socialNetwork.json file for that user.

Note: Read of files should be done using node-fetch.

*/

import fetch from "node-fetch";
import fs from "fs";
import path from "path";
import { INSPECT_MAX_BYTES } from "node:buffer";

// const readData = () => {
//   return fs.promises.readFile(
//     "./userDetails.json",
//     { encoding: "utf8" },
//     (err, data) => {
//       if (err) {
//         console.log(err);
//       } else {
//       }
//     }
//   );
// };
// readData().then((data) => {
//   console.log(data);
// });

//1

function getUserDetails(userName, item) {
  let output = null;
  item.forEach((elem) => {
    if (elem[userName]) {
      output = elem;
    }
  });
  return output ? output : { data: null };
}

const fetchData = () => {
  fetch("http://127.0.0.1:5500/userDetails.json")
    .then((res) => res.json())
    .then((res) => {
      let result = getUserDetails("marcus-alons", res);
      console.log(result);
    });
};
// console.log(fetchData());

//2
function verifyUser(userName, res) {
  return Object.keys(res).reduce((acc, curr) => {
    if (res[curr].includes(userName)) {
      acc[curr] = `user exist in ${curr}`;
    } else {
      acc[curr] = `user does not exist in ${curr}`;
    }
    return acc;
  }, {});
}
const fetchData1 = () => {
  fetch("http://127.0.0.1:5500/socialNetwork.json")
    .then((res) => res.json())
    .then((res) => {
      let result = verifyUser("marcus-alon", res);
      console.log(result);
    })
    .catch((err) => console.log(err));
};
console.log(fetchData1());
