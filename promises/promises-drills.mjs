/*
Q1. Write a function that makes an API call to get result from the following url and display on the console
    https://jsonplaceholder.typicode.com/todos
   
    Note: Add `node-fetch` module

Q2. Dump the result of the first operation in a file in the following format
{ solutionA: result }

Q3. Read the file . Group the result in terms of tasks completed or non completed.
Add the result to the file. (use only reduce)

PS: Do not replace the old result saved in the file.

output after question 3:
{
    solutionA: result
    solutionB:  {
        completed: [],
        noncompleted: []
    }
}

Q4. 

function area (l, b) {
// Write logic for area of a rectangle 
// Promisify the function
}


*/

//1
//node-fetch from v3 is an ESM-only module - you are not able to import it with require().

// import { getPackedSettings } from 'http2';
import fetch from "node-fetch";
import fs from "fs";
// fetch("https://jsonplaceholder.typicode.com/todos")
//   .then((res) => res.json())
//   .then((json) => console.log(json));

//   fetch is a promise so bt 'then' method we can access the value of promise.

//2

function writeData(data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(
      "./data.json",
      JSON.stringify(data),
      { encoding: "utf8" },
      (err) => {
        if (err) reject("err");
        else {
          resolve(data);
        }
      }
    );
  });
}
fetch("https://jsonplaceholder.typicode.com/todos")
  .then((res) => res.json())
  .then((json) => writeData(json));

//3

// const output = fs.readFile("./data.json", "utf8", (err, data) => {
//   // Display the file content
//   if (err) {
//     console.log(err);
//   } else {
//     return data;
//   }
// });
// console.log(output);
// const solutionB = output.reduce(
//   (acc, curr) => {
//     if (curr.completed === true) {
//       return { ...acc, completed: acc.completed.concat(curr) };
//     } else {
//       return { ...acc, noncompleted: acc.noncompleted.concat(curr) };
//     }
//   },
//   {
//     completed: [],
//     noncompleted: [],
//   }
// );
// const result = {
//   solutionA: output,
//   solutionB: solutionB,
// };
// console.log(result);

//4

// function area(l, b) {
//   return new Promise(function (resolve, reject) {
//     resolve(`area of rectangle : ${l * b} sq. units`);
//   });
// }

// area(3, 4).then(function (data) {
//   console.log(data);
// });
