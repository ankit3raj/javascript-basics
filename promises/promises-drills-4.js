/*
Q.1 Promisify fs.readFile and fs.writeFile methods.
Q.2.

    const A = () => Promise.resolve({ data: "A"});
    const B = () => Promise.reject({ error: "B"});
    const C = () => Promise.reject({ error: "C"});
    const D = () => Promise.resolve({ error: "D"});
  
    Note: 
        Throw & catch appropriate errors .
        For problems [A, B, C] Stop calling any other function if error occurs.
       
        A. Perform all operation simultaneously.
        B. Perform D after C after B after A.
        C. Perform A and C together and then call B and afterwards call D.


        D. Perform A and C together and must call (B and afterwards D).
            Catch & handle appropriate errors

        E. Perform A and C together and must call B .
            Don't call D if A or C throws error.

        F.  Perform A and C together, must call D ..
            Don't call B if A throws error.    (alter A to test).

Q.3.

    const A = () => {
        console.log("A called");
        return Promise.reject('error');
    }
    const B = () => {
        console.log('B called')
        return Promise.reject('error');
    }
    const C = () => {
        console.log("C called")
        return Promise.resolve({ data: "C" });
    }
    const D = () => {
        console.log("D called")
        return Promise.resolve({ error: "D" })

   }


  A. Perform A and C together and then call B and afterwards call D.


  B. Perform A and C together and must call (B and afterwards D).
            Catch & handle appropriate errors

  C. Perform A and C together and must call B .
            Don't call D if A or C throws error.

  D.  Perform A and C together, must call D ..
           Don't call B if A throws error.    (alter A to test).





*/

//1
let data = {
  userId: 1,
  id: 5,
  title: "laboriosam mollitia et enim quasi adipisci quia provident illum",
  completed: false,
};
const fs = require("fs");
function writeData(data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(
      "./data3.json",
      JSON.stringify(data),
      { encoding: "utf8" },
      (err) => {
        if (err) reject("err");
        else {
          resolve(data);
        }
      }
    );
  });
}

// writeData(data).then((res) => {
//   console.log(res);
// });

function readData() {
  return new Promise((resolve, reject) => {
    fs.readFile("./data3.json", { encoding: "utf8" }, (err, data) => {
      if (err) reject("err");
      else {
        resolve(data);
      }
    });
  });
}
// readData().then((res) => console.log(res));

//2

// const A = () => Promise.resolve({ data: "A" });
// const B = () => Promise.reject({ error: "B" });
// const C = () => Promise.reject({ error: "C" });
// const D = () => Promise.resolve({ error: "D" });

//A

// Promise.allSettled([A, B, C, D])
//   .then((res) => console.log(res))
//   .catch((err) => console.log(err));

//B
async function callingPromise() {
  try {
    await D();

    await C();
    await B();
    await A();
  } catch (err) {
    console.log("promise rejected");
    console.log(err);
  }
}
// callingPromise();

//C

// Promise.allSettled([A, C])
//   .then((res) => {
//     console.log(res);
//     return B();
//   })
//   .then((res) => {
//     console.log(res);
//     return D();
//   })
//   .then((res) => {
//     consoe.log(res);
//   })
//   .catch((err) => console.log(err));

//D D. Perform A and C together and must call (B and afterwards D).
// Catch & handle appropriate errors

// Promise.allSettled([A, C])
//   .then((res) => {
//     console.log(res);
//     return B(D());
//   })
//   .then((res) => {
//     console.log(res);
//   })
//   .catch((err) => {
//     console.log("rejected");
//     console.log(err);
//   });

//3
const A = () => {
  console.log("A called");
  return Promise.reject("error");
};
const B = () => {
  console.log("B called");
  return Promise.reject("error");
};
const C = () => {
  console.log("C called");
  return Promise.resolve({ data: "C" });
};
const D = () => {
  console.log("D called");
  return Promise.resolve({ error: "D" });
};

//A Perform A and C together and then call B and afterwards call D.

Promise.all([A, C])
  .then((res) => {
    console.log(res);
    return B();
  })
  .then((res) => {
    console.log(res);
    return D();
  })
  .then((res) => {
    console.log("D called");
    console.log(res);
  })
  .catch((err) => {
    console.log("Rejected Promise");
    console.log(err);
  });
