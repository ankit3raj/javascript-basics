/*
Note: For all file operations use fs.promises

Q1. Create 2 files simultaneously.
Wait for 2 seconds and starts deleting them one after another.
(Finish deleting all the files no matter what)


Q2. Create a new file. 
Do File Read and write data to another file
Delete the original file 

    A. using fs.promises chaining
    B. using await keyword


Q3.  
function login(user, val) {
    if(val % 2 === 0) {
        return Promise.resolve(user);
    }
    return Promise.reject(null);
}


function getData() {
    return Promise.resolve("Data");
}

function logData(user, activity) {
    // use fs.promises to save activity in some file
}

Use appropriate method to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    Hint: [Chain/call and await] logData with login function and getData function.
*/

// fs.promise.writefile(path,())

//1
const fs = require("fs");
const data1 = {
  userId: 1,
  id: 1,
  title: "delectus aut autem",
  completed: false,
};
const data2 = {
  userId: 1,
  id: 2,
  title: "quis ut nam facilis et officia qui",
  completed: false,
};

// const WritePromise1 = fs.promises.writeFile(
//   "./data1.json",
//   JSON.stringify(data1),
//   "utf-8",
//   (err) => {
//     if (err) {
//       console.log(err);
//     } else {
//       console.log("data1 created successfully");
//     }
//   }
// );
// const writePromise2 = fs.promises.writeFile(
//   "./data2.json",
//   JSON.stringify(data2),
//   "utf-8",
//   (err) => {
//     if (err) {
//       console.log(err);
//     } else {
//       console.log("data2 created successfully");
//     }
//   }
// );
// Promise.all([writePromise2, WritePromise1])

//   .then((res) => {
//     fs.promises.unlink(
//       "./data1.json",
//       setTimeout(() => console.log("Data1 File Deleted after 2 sec"), 6000)
//     );
//   })
//   .then((res) => {
//     fs.promises.unlink(
//       "./data2.json",
//       setTimeout(() => console.log("Data2 File Deleted after 2 sec"), 9000)
//     );
//   });

//2 A

// const writeData = () => {
//   return fs.promises.writeFile(
//     "./data1.json",
//     JSON.stringify(data1),
//     "utf-8",
//     (err) => {
//       if (err) {
//         console.log(err);
//       } else {
//         console.log("Data1 is created");
//       }
//     }
//   );
// };

// const readData = () => {
//   return fs.promises.readFile("./data1.json", data1, "utf-8", (err, data) => {
//     if (err) {
//       console.log(err);
//     } else {
//       console.log(data);
//       console.log("Reading Data");
//     }
//   });
// };

// const moveFile = () => {
//   return fs.promises.writeFile(
//     "./data2.json",
//     JSON.stringify(data1),
//     "utf-8",
//     (err) => {
//       if (err) {
//         console.log(err);
//       } else {
//         console.log("Data1 is created");
//       }
//     }
//   );
// };

// const deleteFile = () => {
//   return fs.promises.unlink("./data1.json", (err) => {
//     if (err) {
//       console.log(err);
//     } else {
//       console.log("Data1 is deleted");
//     }
//   });
// };

// writeData()
//   .then((res) => {
//     return readData();
//   })
//   .then((res) => {
//     return moveFile();
//   })
//   .then((res) => {
//     return deleteFile();
//   })
//   .then((res) => {
//     console.log(res);
//   });

//2 B

// const writeData = async () => {
//   return await fs.promises.writeFile(
//     "./data1.json",
//     JSON.stringify(data1),
//     "utf-8",
//     (err) => {
//       if (err) {
//         console.log(err);
//       } else {
//         console.log("Data1 is created");
//       }
//     }
//   );
// };

// const readData = async () => {
//   return await fs.promises.readFile(
//     "./data1.json",
//     data1,
//     "utf-8",
//     (err, data) => {
//       if (err) {
//         console.log(err);
//       } else {
//         console.log(data);
//         console.log("Reading Data");
//       }
//     }
//   );
// };

// const moveFile = async () => {
//   return await fs.promises.writeFile(
//     "./data2.json",
//     JSON.stringify(data1),
//     "utf-8",
//     (err) => {
//       if (err) {
//         console.log(err);
//       } else {
//         console.log("Data1 is created");
//       }
//     }
//   );
// };

// const deleteFile = async () => {
//   return await fs.promises.unlink("./data1.json", (err) => {
//     if (err) {
//       console.log(err);
//     } else {
//       console.log("Data1 is deleted");
//     }
//   });
// };
// writeData();
// readData();
// moveFile();
// deleteFile();

//3 A

// async function login(user, val) {
//   if (val % 2 === 0) {
//     await getData();
//     return Promise.resolve(user);
//   }
//   return Promise.reject(null);
// }

// async function getData() {
//   return await Promise.resolve("Data");
// }

// async function logData(user, activity) {
//   // use fs.promises to save activity in some file
//   let userName = await user;
//   let data = { [userName]: activity };

//   return fs.promises.writeFile(
//     "./data1.json",
//     JSON.stringify(data),
//     "utf-8",
//     (err) => {
//       if (err) {
//         console.log(err);
//       } else {
//         console.log("log updated");
//       }
//     }
//   );
// }

// //A

// login("user1", 3)
//   .then((res) => {
//     console.log(res);
//   })
//   .catch((err) => {
//     console.log("Login failed");
//     console.log(err);
//   });

// //B

// login;

function login(user, val) {
  if (val % 2 === 0) {
    return Promise.resolve(user);
  }
  return Promise.reject(null);
}

function getData() {
  return Promise.resolve("Data");
}

function logData(user, activity) {
  // use fs.promises to save activity in some file
  console.log(user, activity);
}

async function loggingData() {
  const val = 2;
  const user = "Sam";
  try {
    await login(user, val);
    logData(user, "Login successful");
    const data = await getData();
  } catch (e) {
    console.log(e, "error coming");
    if (e === null) logData(user, "Login Failure");
  }
}
loggingData();

// Use appropriate method to
// A. login with value 3 and call getData once login is successful
// B. Write logic to logData after each activity for each user. Following are the list of activities
//     "Login Success"
//     "Login Failure"
//     "GetData Success"
//     "GetData Failure"

//     Call log data function after each activity to log that activity in the file.
//     Hint: [Chain/call and await] logData with login function and getData function.
