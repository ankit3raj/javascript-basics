const data1 = {
  34243254: {
    quantity: 3,
    price: 324,
  },
  34243255: {
    quantity: 2,
    price: 310,
  },
  34243256: {
    quantity: 3,
    price: 340,
  },
  34243257: {
    quantity: 1,
    price: 422,
  },
  34243258: {
    quantity: 1,
    price: 354,
  },
};

const dataArr = Object.keys(data1).map((key) => {
  const obj = {};
  obj[key] = data1[key];
  return obj;
});
dataArr.sort((a, b) => a[Object.keys(a)[0]].price - b[Object.keys(b)[0]].price);
// console.log(dataArr);
// console.log(Object.entries(data1));
console.log(dataArr);
