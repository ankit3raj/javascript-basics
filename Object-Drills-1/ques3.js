// Q3. Add a new property to each element of the following array.
// PropertyName -      [adult: true / false  ( age > 18 ? true : false)]

const arr = [
  { name: "Max", age: 32 },
  { name: "Asta", age: 14 },
  { name: "Yuno", age: 16 },
];

const newArr = arr.filter((i) => {
  i.adult = i.age > 18 ? true : false;
  return i;
});
console.log(arr);
