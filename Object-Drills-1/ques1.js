// Q1. Convert the following object into an array.
//  { Max: 32, Asta: 14, Yuno: 16 }
//  to
//  [{ name: "Max", age: 32}, { name: "Asta", age 14}, { name: "Yuno", age: 16}];

//  Use both reduce and map.

const obj = { Max: 32, Asta: 14, Yuno: 16 };

// let arr = [];
// const obj1 = {};
// // for (let key in obj) {
// //   obj1.name = key;
// //   obj1.age = obj[key];
// //   arr.push(obj1);
// // }
// // console.log(arr);

const arr = Object.keys(obj).map((item) => {
  const obj1 = {};
  obj1.name = item;
  obj1.age = obj[item];
  return obj1;
});
// console.log(arr);

// const arr = Object.keys(obj).map((i) => {
//   const obj1 = {};
//   obj1.name = i;
//   obj1.age = obj[i];
//   return obj1;
// });
// console.log(arr);

const arr1 = Object.keys(obj).reduce((acc, curr) => {
  const obj2 = {};
  obj2.name = curr;
  obj2.age = obj[curr];
  acc.push(obj2);
  return acc;
}, []);
console.log(arr1);
