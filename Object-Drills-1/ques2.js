//  Q2.  Convert the following array into an object.
//   [{ name: "Max", age: 32}, { name: "Asta", age 14}, { name: "Yuno", age: 16}];
//   to
//    { Max: 32, Asta: 14, Yuno: 16 }

//    Use reduce.

const arr = [
  { name: "Max Payne", age: 32 },
  { name: "Asta Young", age: 14 },
  { name: "Yuno mario", age: 16 },
];

// let obj1 = {};
// for (let elem in arr) {
//   obj1[arr[elem].name] = arr[elem].age;
// }
// console.log(obj1);

// const arrayToObject = arr.reduce((acc, curr) => {
//   acc[curr.name] = curr.age;
//   return acc;
// }, {});
// console.log(arrayToObject);
// for (let elem of arr) {
//   let res = elem.name.split(" ")[0];
//   console.log(res);
// }
// console.log();
const result = arr.reduce((acc, curr) => {
  //   const name = curr.name;
  const obj = { name: { first: "", last: "" }, age: "" };
  //   obj.first = curr.name.split(' ')[0];
  //   obj.last = curr.name.split(" ")[1];
  obj.name.first = curr.name.split(" ")[0];
  obj.name.last = curr.name.split(" ")[1];
  obj.age = curr.age;
  acc.push(obj);

  return acc;
}, []);
console.log(result);
// {{ first: 'Max', last: 'Payne' }, age:18}
// const myarr = [];
// const obj = { name: { first: "", last: "" }, age: "" };
// for (let elem of arr) {
//   obj.name.first = elem.name.split(" ")[0];
//   obj.name.last = elem.name.split(" ")[1];
//   obj.age = elem.age;
//   myarr.push(obj);
//   console.log(obj);
// }
// console.log(myarr);

// const result = arr.reduce((acc,curr) => {
//     const obj
// })
