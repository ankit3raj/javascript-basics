// Q4. const items = [
//     { qty: 23, name: "Microphone", price: "34USD" },
//     { qty: 4, name: "HeadSet", price: "14USD" }
//     { qty: 2, name: "Camera", price: "48USD" }
// ]
// A.  Use above `items` array to generate the below array
//     Use reduce and map.
//     const result = [
//         { name: "Microphone", price: "34USD" },
//         { name: "HeadSet", price: "14AUD" }
//         { name: "Camera", price: "48EUR" }
//     ]

// B. Use above `items` array to generate the following object.
//     const result = {
//         Microphone: {
//             price: {
//                 amount: 34,
//                 currency: "USD"
//             }
//         },
//         HeadSet: {
//             price: {
//                 amount: 14
//                 currency: "AUD"
//             }
//         },
//         Camera: {
//             price: {
//                 amount: 48,
//                 currency: "EUR"
//             }
//         }
//     }

// A
const items = [
  { qty: 23, name: "Microphone", price: "34USD" },
  { qty: 4, name: "HeadSet", price: "14USD" },
  { qty: 2, name: "Camera", price: "48USD" },
];

// const omit = (obj, arr) =>
//   Object.keys(obj)
//     .filter((k) => !arr.includes(k))
//     .reduce((acc, key) => ((acc[key] = obj[key]), acc), {});
// console.log(omit(items, ));
// console.log(items[0].qty);
// const omit = items.map((i) => {
//   i.delete(i.qty);
//   return i;
// });
// console.log(omit);

//B
const results = {
  Microphone: {
    price: {
      amount: 34,
      currency: "USD",
    },
  },
  HeadSet: {
    price: {
      amount: 14,
      currency: "AUD",
    },
  },
  Camera: {
    price: {
      amount: 48,
      currency: "EUR",
    },
  },
};

const splittingCurrencyFromPrice = (price) => {
  let result = { currency: "", amount: "" };
  for (let i = 0; i < price.length; i++) {
    let x = parseInt(price[i], 10);
    if (isNaN(x)) {
      result.currency += price[i];
    } else {
      result.amount += x.toString();
    }
  }
  return {
    currency: result.currency,
    amount: parseInt(result.amount || 0),
  };
};

console.log(
  items.reduce((acc, curr) => {
    const name = curr.name;
    const { currency, amount } = splittingCurrencyFromPrice(curr.price);
    acc[name] = {
      price: {
        currency: currency,
        amount: amount,
      },
    };
    return acc;
  }, {})
);
