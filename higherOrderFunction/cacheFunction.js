// Should return a function that invokes `cb`.
// A cache (object) should be kept in closure scope.
// The cache should keep track of all arguments have been used to invoke this function.
// If the returned function is invoked with arguments that it has already seen
// then it should return the cached result and not invoke `cb` again.
// `cb` should only ever be invoked once for a given set of arguments.

function cacheFunction(cb) {
  const cache = {};
  return function () {
    console.log(arguments);
    const key = Array.from(arguments).join("===");
    if (cache[key]) {
      console.log("from cache");
      return cache[key];
    } else {
      console.log("fresh computation");
      const res = cb.apply(null, Array.from(arguments));
      cache[key] = res;
      return res;
    }
  };
}

const cb1 = (n1, n2) => n1 + n2;
const f1 = cacheFunction(cb1);
const sum1 = f1(2, 3);
const sum2 = f1(2, 3);
console.log(sum1, sum2);
