// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned

function limitFunctionCallCount(cb, n) {
  return function () {
    if (n > 0) {
      for (let i = 0; i < n; i++) {
        cb();
      }
    } else {
      return null;
    }
  };
}

const cb = () => console.log("Hello world");

const callCount = limitFunctionCallCount(cb, 0);
console.log(callCount());





// function limitFunctionCallCount(cb, n) {
//   let count = 0;
//   return function () {
//     count++;
//     cb();
//   };
// }

// function cb() {}
// const callCount = limitFunctionCallCount(cb, 0);
// console.log(callCount());
