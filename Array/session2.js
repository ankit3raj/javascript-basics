// 1. Create an array to store colors of a rainbow and display the result using a loop.

let arr1 = ["voilet", "indigo", "blue", "green", "yellow", "orange", "red"];
for (let i = 0; i <= arr.length; i++) {
  console.log(arr[i]);
}

// const emps = [{ age: 23, name: "Mike"}, { age: 22, name : "Julius"}]
// Use a loop to display data of each employee.

var emps = [
  { age: 23, name: "Mike" },
  { age: 22, name: "Julius" },
];
for (let elem of emps) {
  console.log(elem);
}

// 3.const ob = { max:  9833293293, george: 992324434, orion: 9293243288 };
// Write a function that takes a name as input parameter and display corresponding mobile number.
// A. Use Switch statement.

function phnNo(name) {
  const ob = { max: 9833293293, george: 992324434, orion: 9293243288 };
  switch (name) {
    case "max":
      console.log(ob.max);
      break;
    case "george":
      console.log(ob.george);
      break;
    case "orion":
      console.log(ob.orion);
      break;
  }
}

// B. Use loops.
function phnNo(name) {
  const ob = { max: 9833293293, george: 992324434, orion: 9293243288 };
  for (let [key, value] of Object.entries(ob)) {
    if (key === name) {
      return value;
    }
  }
}
console.log(phnNo("max"));

// 4. Concatenate two strings "Max", "Payne".

console.log("Max" + "Payne");

// 5. Split the following strings based on spaces.
// "Max Spectre Payne"
let gameCharacter = "Max Spectre Payne";
console.log(gameCharacter.split(" "));

// 6. Split each digit from "123456" and store them in an array.

let num = "123456";
let arr = num.split("");
console.log(arr);
