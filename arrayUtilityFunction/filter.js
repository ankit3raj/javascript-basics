// Do NOT use .filter, to complete this function.
// Similar to `find` but you will return an array of all elements that passed the truth test
// Return an empty array if no elements pass the truth test
function cb(elements, element) {
  let count = 0;
  for (let i = 0; i < elements.length; i++) {
    if (elements[i] == element) {
      return "true";
    }
  }
}
function filter(elements, cb) {
  let result = "";
  let arr = [];
  for (let i = 0; i < elements.length; i++) {
    result = cb(elements, elements[i]);
    if (result == "true") {
      arr.push(elements[i]);
    }
  }
  console.log(arr);
}

filter([1, 2, 3, 4, 5, 5], cb);
