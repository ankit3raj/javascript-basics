// Flattens a nested array (the nesting can be to any depth).
// Hint: You can solve this using recursion.
// Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

function flatten(elements) {
  let flat = [];
  for (let i = 0; i < elements.length; i++) {
    if (Array.isArray(elements[i])) {
      flat.push(...flatten(elements[i]));
    } else {
      flat.push(elements[i]);
    }
  }
  return flat;
}
console.log(flatten([1, [2], [[3]], [[[4]]]]));
