// Do NOT use .map, to complete this function.
// How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
// Produces a new array of values by mapping each value in list through a transformation function (iteratee).
// Return the new array.

function cb(element) {
  return element * element;
}

function map(elements, cb) {
  let result = [];
  for (var i = 0; i < elements.length; i++) {
    result.push(cb(elements[i]));
  }
  return result;
}

console.log(map([1, 2, 3, 4, 5, 5], cb));
