// Do NOT use .includes, to complete this function.
// Look through each value in `elements` and pass each element to `cb`.
// If `cb` returns `true` then return that element.
// Return `undefined` if no elements pass the truth test.
function cb(elements, element) {
  for (let i = 0; i < elements.length; i++) {
    if (elements[i] === element) {
      return true;
    }
  }
  return;
}
function find(elements, cb) {
  let result = "";
  for (let i = 0; i < elements.length; i++) {
    result = cb(elements, elements[i]);
    if (result) {
      return elements[i];
    }
  }
}

find([1, 2, 3, 4, 5, 5], cb);
