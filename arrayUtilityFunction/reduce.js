// Do NOT use .reduce to complete this function.
// How reduce works: A reduce function combines all elements into a single value going from left to right.
// Elements will be passed one by one into `cb` along with the `startingValue`.
// `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
// `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.

function cb(result, element, i, elements) {
  result = result + element;
  return result;
}

function reduce(elements, cb, startingValue) {
  let result = "";
  if (startingValue || startingValue === undefined) {
    result = startingValue;
    for (let i = 0; i < elements.length; i++) {
      result = cb(result, elements[i], i, elements);
    }
  } else {
    result = elements[0];
    for (let i = 1; i < elements.length; i++) {
      result = cb(result, elements[i], i, elements);
    }
  }
  return result;
}

console.log(reduce([1, 2, 3, 4, 5, 5], cb, 0));
