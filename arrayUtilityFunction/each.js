// Do NOT use forEach to complete this function.
// Iterates over a list of elements, yielding each in turn to the `cb` function.
// This only needs to work with arrays.
// You should also pass the index into `cb` as the second argument
// based off http://underscorejs.org/#each

const items = [1, 2, 3, 4, 5, 5];

function cb(element) {
  return element;
}

function each(elements, cb) {
  let result = "";
  for (var i = 0; i < elements.length; i++) {
    result = cb(elements[i]);
  }
  return result;
}

each([1, 2, 3, 4, 5, 5], cb);
