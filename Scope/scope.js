const first = "ankit"; // Global Variable
// In the browser ,The global scope is called the window. Methods are globally available to us through window.
//
let second = "raj";
var age1 = 100;
// if we do the window.first or window.second ,it will give undefined but if we do window.age ,it will give the value because var variables are attached to the window object and that are globally scoped.const and let are also globally scoped but not attached to the windows.
function sayHi() {
  console.log("hi");
}
// function when declared globally are available inside the windows.

const age = 100;

// function go() {
//   const hair = "blonde";
// }
// go();
// console.log(age);
// console.log(hair);

//hair value is not coming
// functional scope - when variables are created inside of a function,those variables are available only inside of that function.it will not be available outside of that function.

//scope -look up - if a variable is not inside of that function ,it will go up a level higher and look for that variable.
function go() {
  const hair = "blonde";
  const age = 200;
  console.log(age);
}
go();

console.log(hair);

// here age is refereed to as shadow variables.we can name the same variable in different scope but that is not best practices.

// const variables can't be reassigned but let and var variable can be reassigned.

if (1 == 1) {
  var cool = true;
}
console.log(cool);

// to access cool outside of a block either i have to use var keyword or we have to declare variable outside of a block.
