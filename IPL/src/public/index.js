const fetchData = () => {
  fetch("./output/matchesPerSeason.json")
    .then((res) => res.json())
    .then((res) => {
      console.log(res);
      generateChart(res);
    });
};

function generateChart(data) {
  const transformData = Object.keys(data).map((item) => {
    return [item, data[item]];
  });
  Highcharts.chart("match-won", {
    chart: {
      type: "column",
    },
    title: {
      text: "Number of matches played per year for all the years in IPL",
    },

    xAxis: {
      type: "category",
      labels: {
        rotation: -45,
        style: {
          fontSize: "13px",
          fontFamily: "Verdana, sans-serif",
        },
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: "Matches Played",
      },
    },
    legend: {
      enabled: false,
    },
    tooltip: {
      pointFormat: "Number of Matches Played: <b>{point.y:.1f} per year</b>",
    },
    series: [
      {
        name: "Population",
        data: transformData,
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: "#FFFFFF",
          align: "right",
          format: "{point.y:.1f}", // one decimal
          y: 10, // 10 pixels down from the top
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif",
          },
        },
      },
    ],
  });
}

let output = fetchData();
// console.log(output);
//3
const fetchData1 = () => {
  fetch("./output/extraRunPerTeam.json")
    .then((res) => res.json())
    .then((res) => {
      console.log(res);
      generateChart1(res);
    });
};
function generateChart1(data) {
  const transformData = Object.keys(data).map((item) => {
    return [item, data[item]];
  });
  Highcharts.chart("extra-run", {
    chart: {
      type: "column",
    },
    title: {
      text: "Extra Runs conceded per team ",
    },

    xAxis: {
      type: "category",
      labels: {
        rotation: -45,
        style: {
          fontSize: "13px",
          fontFamily: "Verdana, sans-serif",
        },
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: "Matches Played",
      },
    },
    legend: {
      enabled: false,
    },
    tooltip: {
      pointFormat: "Number of Matches Played: <b>{point.y:.1f} per year</b>",
    },
    series: [
      {
        name: "Population",
        data: transformData,
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: "#FFFFFF",
          align: "right",
          format: "{point.y:.1f}", // one decimal
          y: 10, // 10 pixels down from the top
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif",
          },
        },
      },
    ],
  });
}

let output1 = fetchData1();
// console.log(output);
//4 top 10 bowler

const fetchData2 = () => {
  fetch("./output/top10Bowler.json")
    .then((res) => res.json())
    .then((res) => {
      console.log(res);
      generateChart2(res);
    });
};
function generateChart2(data) {
  const transformData = data.map((item) => {
    return [item.bowlerName, parseInt(item.economy)];
  });
  Highcharts.chart("top10-bowler", {
    chart: {
      type: "column",
    },
    title: {
      text: "Top 10 Bowler ",
    },

    xAxis: {
      type: "category",
      labels: {
        rotation: -45,
        style: {
          fontSize: "13px",
          fontFamily: "Verdana, sans-serif",
        },
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: "Matches Played",
      },
    },
    legend: {
      enabled: false,
    },
    tooltip: {
      pointFormat: "Number of Matches Played: <b>{point.y:.1f} per year</b>",
    },
    series: [
      {
        name: "Population",
        data: transformData,
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: "#FFFFFF",
          align: "right",
          format: "{point.y:.1f}", // one decimal
          y: 10, // 10 pixels down from the top
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif",
          },
        },
      },
    ],
  });
}

let output2 = fetchData2();

//2 matches win per season per team
const fetchData3 = () => {
  fetch("./output/matchesWinPerSeasonPerTeam.json")
    .then((res) => res.json())
    .then((res) => {
      console.log(res);
      generateChart3(res);
    });
};
function generateChart3(data) {
  const xAxisData = Object.keys(data);
  let allTeams = {};
  Object.values(data).forEach((curr) => {
    Object.keys(curr).forEach((x) => {
      if (!allTeams[x]) {
        allTeams[x] = [];
      }
    });
  });
  Object.values(data).forEach((item) => {
    Object.keys(allTeams).forEach((x) => {
      allTeams[x].push(item[x] || 0);
    });
  });

  const finalyAxis = Object.keys(allTeams).reduce((acc, curr) => {
    let obj = {};
    obj.name = curr;
    obj.data = allTeams[curr];
    acc.push(obj);
    return acc;
  }, []);

  Highcharts.chart("matches-win", {
    chart: {
      type: "column",
    },
    title: {
      text: "Team win per season",
    },

    xAxis: {
      categories: xAxisData,
      crosshair: true,
    },
    yAxis: {
      min: 0,
      title: {
        text: "Matches Won",
      },
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat:
        '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
      footerFormat: "</table>",
      shared: true,
      useHTML: true,
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0,
      },
    },
    series: finalyAxis,
  });
}

let output3 = fetchData3();
