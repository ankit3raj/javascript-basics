const { Console } = require("console");
const fs = require("fs");

const data = fs.readFileSync("../data/matches.csv", {
  encoding: "utf8",
});
const convertToJson = (data) => {
  const lines = data.split("\n");
  const keys = lines[0].split(",");
  const restData = lines.slice(1).filter((line) => line.length);
  const jsonArr = restData.map((eachLine) => {
    const values = eachLine.split(",");
    const jsonObj = {};
    values.forEach((eachValue, index) => {
      jsonObj[keys[index]] = eachValue;
    });
    return jsonObj;
  });
  return jsonArr;
};
let workingData = convertToJson(data);
const getMatchesPerSeason = (result) => {
  const matchPlayedPerSeason = {};
  result.forEach((item) => {
    if (matchPlayedPerSeason[item.season]) {
      matchPlayedPerSeason[item.season]++;
    } else {
      matchPlayedPerSeason[item.season] = 1;
    }
  });

  return matchPlayedPerSeason;
};
const finalResult = getMatchesPerSeason(workingData);
// fs.writeFileSync(
//   "../public/output/matchesPerSeason.json",
//   JSON.stringify(finalResult),
//   { encoding: "utf8" }
// );

const data1 = fs.readFileSync("../data/deliveries.csv", {
  encoding: "utf8",
});
const convertToJson1 = (data1) => {
  const lines1 = data1.split("\n");
  const keys1 = lines1[0].split(",");
  const restData1 = lines1.slice(1).filter((line) => line.length);
  const jsonArr1 = restData1.map((eachLine1) => {
    const values1 = eachLine1.split(",");
    const jsonObj1 = {};
    values1.forEach((eachValue, index) => {
      jsonObj1[keys1[index]] = eachValue;
    });
    return jsonObj1;
  });
  return jsonArr1;
};
const deliveries = convertToJson1(data1);

// const getWinMatchesPerTeamPerSeason = (result) => {
//   const winMatchPerTeamPerSeason = {};
//   result.forEach((item) => {
//     winMatchPerTeamPerSeason[]
//   }).reduce()

//   return matchPlayedPerSeason;
// };
// 2
const getmatchesWonPerTeamPerYear = (result) => {
  let matchesWon = result.reduce((acc, curr) => {
    if (acc[curr.season]) {
      if (acc[curr.season][curr.winner]) {
        acc[curr.season][curr.winner]++;
      } else {
        acc[curr.season][curr.winner] = 1;
      }
    } else {
      acc[curr.season] = {};
      acc[curr.season][curr.winner] = 1;
    }
    return acc;
  }, {});
  // console.log(matchesWon);
  return matchesWon;
};

const matchesWonPerSeason = getmatchesWonPerTeamPerYear(workingData);
console.log(matchesWonPerSeason);
// fs.writeFileSync(
//   "../public/output/matchesWinPerSeasonPerTeam.json",
//   JSON.stringify(matchesWonPerSeason),
//   { encoding: "utf8" }
// );

// 3. Extra runs conceded per team in the year 2016

const getId = (result) => {
  const obj = [];
  const res = result.filter((y) => {
    if (y.season === "2016") {
      obj.push(y.id);
    }
  });
  return obj;
};
const myId = getId(workingData);
let extraRun = deliveries.reduce((acc, curr) => {
  if (myId.includes(curr.match_id)) {
    if (acc[curr.bowling_team]) {
      acc[curr.bowling_team] += parseInt(curr.extra_runs);
    } else {
      acc[curr.bowling_team] = parseInt(curr.extra_runs);
    }
  }
  return acc;
}, {});

// fs.writeFileSync(
//   "../public/output/extraRunPerTeam.json",
//   JSON.stringify(extraRun),
//   { encoding: "utf8" }
// );

// 4.Top 10 economical bowlers in the year 2015

let getIdsOf2015 = (data) => {
  let array = [];

  for (let item of data) {
    if (item.season === "2015") {
      array.push(item.id);
    }
  }
  return array;
};

const arrayOfIds2015 = getIdsOf2015(workingData);

const totalDeliveries2015 = deliveries.filter(
  (item) =>
    item.match_id >= +arrayOfIds2015[0] &&
    item.match_id <= +arrayOfIds2015[arrayOfIds2015.length - 1]
);

const bowlersOf2015 = (deliveries2015) => {
  return deliveries2015.reduce((acc, curr) => {
    if (acc[curr.bowler]) {
      acc[curr.bowler].balls++;
      acc[curr.bowler].runs += +curr.total_runs;
    } else {
      acc[curr.bowler] = { balls: 1, runs: +curr.total_runs };
    }
    return acc;
  }, {});
};

const allBowlersData = bowlersOf2015(totalDeliveries2015);

const findEconomy = (bowlers) => {
  // {bName, economy};
  let economy = (balls, runs) => ((runs / balls) * 6).toFixed();
  let result = [];
  Object.keys(bowlers).forEach((i) => {
    result.push({
      bowlerName: i,
      economy: economy(bowlers[i].balls, bowlers[i].runs),
    });
  });
  return result;
};

const bowlersNameWithEconomy = findEconomy(allBowlersData);

let top10EconomicBowlers = bowlersNameWithEconomy
  .sort((a, b) => a.economy - b.economy)
  .slice(0, 10);
// fs.writeFileSync(
//   "../public/output/top10Bowler.json",
//   JSON.stringify(top10EconomicBowlers),
//   { encoding: "utf8" }
// );
