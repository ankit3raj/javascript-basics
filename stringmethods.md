# String Methods

1. 1.charAt(indexOfCharacter) method: This method returns the character at the specified index. String in JavaScript has zero-based indexing.
2. Parameters: This method accepts single parameter indexOfCharacter that holds the Index of the character of any string.

3. 2.charCodeAt(indexOfCharacter) Method: This method returns a number that represents the Unicode value of the character at the specified index. This method accepts one argument.

4. 3.concat( objectOfString ) Method: This method combines the text of two strings and returns a new combined or joined string. To concatenate two strings, we use concat() method on one object of string and send another object of string as a parameter. This method accepts one argument. The variable contains text in double quotes or single quotes

5. 4.endsWith(queryString, length) Method: This methods checks whether a string ends with a specified string or characters. This method returns “true” if string ends with the provided string, else it returns “false”. This method is case sensitive. This method accepts two arguments.
6. queryString: The string to be searched for.
7. length: The default value is the length of the string you provided.

8. 5.fromCharCode(UNICODE_NUMBER) Method: This method converts UNICODE values to characters. This is a static method of the String object. The first method is not starting with the string variable. This method returns character for the given specific UNICODE. This method accepts a single parameter UNICODE-NUMBER that holds the number for which you want character.

9. 6.includes(queryString) Method: This method checks whether the string variable contains specific string or not. This method returns “true” if the string is present in the variable string variable, else it returns “false”. This method is case-sensitive. This method accepts a single parameter queryString that holds the string that you want to check if it is present or not.

10. 7.indexOf(queryString) Method: This method returns the index of first occurrence of given query string. This method returns -1, if given character or string is not present in string variable. This method is case-sensitive. This method accepts single parameter queryString that holds the Character or string for getting index of that string.

11. 8.repeat(number) Method: This method returns string with the number of copies of the existing string. This method accepts single parameter number that holds the number of copies that you want for a existing string.

12. 9.replace(replaceValue, replaceWithValue) Method: This method returns string with the changes. This method is case-sensitive.This method accepts two parameters as mentioned above and described below:
13. replacedValue: This parameter holds the character that you want to replace.
14. replaceWithValue: This parameter holds the character that you want to replace with.

15. 10.search(queryString) Method: This method search for specified value or regular expression. This method returns the position of the match if it is found and if it is not found, it returns -1. This method is case-sensitive. This method accepts a single parameter queryString that holds the string that you want to get the position.

16. 11.slice(startIndex, endIndex) Method: This method extract a part of string and returns a new string
17. This method accepts two arguments.
18. startIndex: This parameter holds the index from where you want to extract. It is included.
19. endIndex: This parameter holds the index till where you want to extract. It is excluded.

20. 12.split(character) Method: This method splits the string into an array of sub-strings. This method returns an array. This method accepts single parameter character on which you want to split the string.

21. 13.startsWith(queryString) Method: This method checks if a string starts with a given query string or not. This method returns “true” if string starts with provided query string else it returns “false”. This method accepts single parameter queryString that you want to check existing string start with it or not.

22. 14.toLowerCase(stringVariable) Method: This method converts all the character present in string to lower case and returns a new string with all the characters in lower case. This method accepts single parameter stringVariable string that you want to convert in lower case.

23. 15.toUpperCase(stringVariable) Method: This method converts all the character present in String to upper case and returns a new String with all character in upper case. This method accepts single parameter stringVariable string that you want to convert in upper case.

24. 16.trim() Method: This method is used to remove either white spaces from the given string. This method returns a new string with removed white spaces. This method called on a String object. This method doesn’t accept any parameter.
