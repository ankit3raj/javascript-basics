// Hoisting in javascript allows us to access function and variable before they have been created .
// There are two things in javascript that are hoisted , functional and variable declaration.
sayHi();
function sayHi() {
  console.log("hey");
  console.log(add(10, 2));
}
// It will work beacuase in javascript beacuse when you run your javascript file ,javascript the compiler will take all of our function declaration to the top. so in hoisting ,we can technically run a function before it exists.

function add(a, b) {
  return a + b;
}
// this will work
