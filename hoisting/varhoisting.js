console.log(age);
var age = 10;

// variable hoisting
// In javascript compiler ,this will work like -
var age;
console.log(age);
age = 10;

// so ,age is declared but not initialized .

const add = function (a, b) {
  return a + b;
};
// this is not a functional declaration,this is a function stuck in a variable.
//  Hoisting only works with regular function.
